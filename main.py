from browser import document, window, local_storage
from core.youtube_parser import YoutubeVideoParser

def show_url(e):
	currentUrl = local_storage.storage['currentUrl']
	yvp = YoutubeVideoParser(currentUrl)
	document["show"].textContent = yvp.get_video_id()

document["button"].bind("click", show_url)