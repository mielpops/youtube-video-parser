/*
	Loading brython using click event 
	Report : 
		- onload doesn't work
	Resolved : 
		- it now works 
*/
window.onload = function(){
	brython({debug: 1});
}

document.addEventListener('DOMContentLoaded', () => {
	
	let queryInfo = {
		active : true,
		currentWindow: true
	};

	chrome.tabs.query(queryInfo, (tabs) =>{
		localStorage.setItem('currentURL', tabs[0].url);
	});
});