# Youtube Video Parser (YVP)

##### **Project presentation :**
Youtube video parser aims to parse youtube subtitles, it gives the possibility for users to search for occurence on youtube videos.

##### **Goals :**
- Get subtitles
    - google API -> problems 
        - refer to **know issues**
    - reversed engineered youtube requests
        - get obfuscate url request url 
        - deobfuscate url and get this link -> https://video.google.com/timedtext?lang={INSERT LANG}&v={YOUTUBE VIDEO ID}
    - request previous link 
    - parse HTML response with beautifulsoup4 
- Parse subtitles
- Give the possibilty for users to search for occurence

##### **Install :**
Run 
```
    make install
```

##### **Know Issues :**
    - Cannot parse auto-generated captions
        - need to parse every avalaible language -> 7 languages
    - Cannot run brython for web extension integration -> resolved 
        - onload event does not work
    - Cannot use google API 
        - need to be video author
        - need author authorization 
    - HTML beautifulsoup latin encoding issue

#### **To do :**
- Rename YoutubeVideoParser -> not parsing but formatting or implement parser on YoutubeVideoParser directly
- Create extension user interface
- Create client side script with brython


